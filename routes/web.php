<?php


Route::get('/','HomeController@getHomePage');
Route::post('/available-bus','HomeController@availableBus');
Route::get('/bus-booking/{id}','HomeController@getBusBooking');
Route::post('/confirm-ticket','HomeController@postConfirmTicket');
Route::get('/confirm-payment/{id}','HomeController@getConfirmPayment');
Route::post('/confirm-payment','HomeController@postConfirmPayment');
Route::post('/payment-verification','HomeController@paymentVerification');

Route::get('/verify-payment','HomeController@getVerifyPayment');
Route::post('/verify-payment','HomeController@postVerifyPayment');



Route::get('/access','AdminController@getAdminLogin');
Route::post('/access','AdminController@postAdminLogin');

Route::get('/access/register','AdminController@getAdminRegister');
Route::post('/access/register','AdminController@postAdminRegister');

Route::group(['middleware' => ['admin']], function () {
	Route::get('/access/logout','AdminController@getAdminLogout');
	Route::get('/access/home','HomeController@getHome');
	
	Route::get('/access/add-bus','AddBusController@getAddBus');
	Route::post('/access/add-bus','AddBusController@postAddBus');
    
	Route::get('/access/bus-route','AddBusController@getBusRoute');
	Route::post('/access/bus-route','AddBusController@postBusRoute');

    Route::get('/access/buses','AddBusController@getBuses');
	
	
	
	
});

