@extends('/v1/layout')

@section('title')
    Home
@stop
<style>
    select#bus_type {
        height: 23px;
    }
</style>
@section('body')
	<br>
    <h2>Bus Route</h2>
    {{ session('error_message')}}
    {{ session('success_message')}}
	<div class="row">
		<div class="col-md-6">
			<p>Bus Route</p>
			<form role="form" method="post" action="{{ url('/access/bus-route') }}">
			{{ csrf_field() }}

				<span class="error">{{ $errors->first('category_name') }}</span>
				<div class="form-horizontal biller">
					
					<div class="form-group">
						<input type="text" class="form-control" id="route_name" name="route_name" placeholder="Route Name" value="{{old('route_name')}}">
                    </div>
					<div class="form-group">	
						<input type="text" class="form-control" id="from" name="from" placeholder="From" value="{{old('from')}}">
					</div>
					<div class="form-group">	
						<input type="text" class="form-control" id="route_name" name="to" placeholder="To" value="{{old('to')}}">
					</div>
					
					<div class="form-group">
						<input type="text" class="form-control" id="route_through" name="route_through" placeholder="Route Through" value="{{old('route_through')}}">
					</div>
					
					
					<div class="form-group">
						<input type="number" class="form-control" id="distance" name="distance" placeholder="Distance" value="{{old('distance')}}">
					</div>
					
					
					<div class="form-group">
						<input type="submit" value="Add" class="form-control" name="submit" style="cursor:pointer;background: #33b5e5;color: #fff;">
					</div>
					
				</div>
			</form>
		</div>
		
		<div class="col-md-6">
			<p>&nbsp;</p>
			<ul>
			    @foreach($routes as $route)
			            <li>{{$route->route_name}} [{{$route->distance}} km]</li>
			    @endforeach
			</ul>
		</div>
		
		
		
		
		
	</div>
    
	
	
	<div style="float:right;">
		
	</div>
    <div class="page">
      
    </div>
  </div>
  
</div>

@stop


@section('script')
	<script>
		$(document).ready(function(){
			
			var data = '<tr class="">'+
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td><a href="#" onClick="return confirm(\'Are you really want to delete this item?\')" id="remove"><i class=\'fa fa-trash-o\'></i></a></td>'+
        '</tr>';
			$("#add_another").click(function(){
				$("tbody").append(data);
				
			});
			
			
			$(".table").on('click', '#remove', function () {
				$(this).closest('tr').remove();
			});
			
		});
		
		
	</script>
@stop
