<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>GirlyShopper | Admin Login & Register</title>
    <link rel="stylesheet" href="/v1/css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="/v1/css/style.css">
  </head>


<body>
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->

<!-- Form Module-->
<div class="module form-module">
  <a href="/access"><div class="toggle"><i class="fa fa-times fa-user-plus"></i>
    <div class="tooltip">Admin Login</div>
  </div>
  </a>
  
  <div class="form">
    <h2>Create an account</h2>
	{{$success_message or ''}}
	{{$error_message or ''}}
    <form action="/access/register" method="post" enctype="multipart/form-data">
	  {{csrf_field()}}
	  {{ $errors->first('full_name') }}
      <input type="text" name="full_name" placeholder="Full Name"/>
      
	  {{ $errors->first('username') }}
	  <input type="text" name="username" placeholder="Username"/>
      
	  {{ $errors->first('avatar') }}
	  <input type="file" name="avatar"/>
      
	  {{ $errors->first('password') }}
	  <input type="password" name="password" placeholder="Password"/>
      
	  {{ $errors->first('re_password') }}
	  <input type="password" name="re_password" placeholder="Re-Enter Password"/>
      <button>Register</button>
    </form>
  </div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src='/v1/js/da0415260bc83974687e3f9ae.js'></script>
	<script src="/v1/js/index.js"></script>

    
    
    
  </body>
</html>