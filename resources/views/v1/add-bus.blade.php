@extends('/v1/layout')

@section('title')
    Home
@stop
<style>
    select#bus_type {
        height: 23px;
    }
</style>
@section('body')
	<br>
    <h2>Add Bus</h2>
    {{ session('error_message')}}
    {{ session('success_message')}}
	<div class="row">
		<div class="col-md-6">
			<p>Add Bus</p>
			<form role="form" method="post" action="{{ url('/access/add-bus') }}">
			{{ csrf_field() }}

				<span class="error">{{ $errors->first('category_name') }}</span>
				<div class="form-horizontal biller">
					
					<div class="form-group">
						<input type="text" class="form-control" id="bus_model" name="bus_model" placeholder="Bus Model Name" value="{{old('bus_model')}}">
					</div>
					<div class="form-group">
						<select class="form-control" name="bus_type" id="bus_type">
						    <option value="">Select Bus Type</option>
						    <option value="AC">AC</option>
						    <option value="AC">None AC</option>
						    <option value="AC">Business</option>
						    <option value="AC">Elite</option>
						</select>
					</div>
					
					<div class="form-group">
					    <select class="form-control" name="bus_route" id="bus_route">
                            @foreach($routes as $route)
                            <option value="{{$route->id}}"><li>{{$route->route_name}}</li></option>
                            @endforeach
                        </select>
					</div>
					
					<div class="form-group">
						<input type="date" class="form-control" id="category_name" name="journey_date" placeholder="Journey Date" value="{{old('journey_date')}}">
					</div>
					
					<div class="form-group">
						<input type="time" class="form-control" id="depture_time" name="depture_time" placeholder="Depture Time" value="{{old('depture_time')}}">
					</div>
					
					<div class="form-group">
						<input type="time" class="form-control" id="arrival_time" name="arrival_time" placeholder="Arrival Time" value="{{old('arrival_time')}}">
					</div>
					
					
					<div class="form-group">
						<input type="number" class="form-control" id="fare" name="fare" placeholder="Fare" value="{{old('fare')}}">
					</div>
					
					
					<div class="form-group">
						<input type="submit" value="Add" class="form-control" name="submit" style="cursor:pointer;background: #33b5e5;color: #fff;">
					</div>
					
				</div>
			</form>
		</div>
		
		<div class="col-md-6">
			<p>&nbsp;</p>
			<ul>
			
			</ul>
		</div>
		
		
		
		
		
	</div>
    
	
	
	<div style="float:right;">
		
	</div>
    <div class="page">
      
    </div>
  </div>
  
</div>

@stop


@section('script')
	<script>
		$(document).ready(function(){
			
			var data = '<tr class="">'+
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td><a href="#" onClick="return confirm(\'Are you really want to delete this item?\')" id="remove"><i class=\'fa fa-trash-o\'></i></a></td>'+
        '</tr>';
			$("#add_another").click(function(){
				$("tbody").append(data);
				
			});
			
			
			$(".table").on('click', '#remove', function () {
				$(this).closest('tr').remove();
			});
			
		});
		
		
	</script>
@stop
