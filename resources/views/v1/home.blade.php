@extends('/v1/layout')

@section('title')
    Cash Book View
@stop
@section('body')
	<style type="text/css">
		.table > tbody > tr > td {
    		padding: 0px 1px;
    	}
    	.table > tbody > tr{
		    border: 0px solid;
		    font-size: 13px;
		}
		.table > tbody > tr > td {
		    padding: 0px 1px;
		    border: 0px solid;
		    border-bottom: 1px solid #efecec;
		    font-size: 13px;
		}
		.table {
		    border: 0px solid #33B5E5;
		}
		.dbx{
			padding: 5px;
		}
		.dbx p{
			text-align: center;
			margin-bottom: 0px;
		}.dbx i{
			padding: 5px;
		}
		.table > thead > tr > th {
		    color: #666;
		    padding: 0px 5px;
		    text-align: left;
		    background: #fff;
		}
		.table td:last-child, .table th:last-child {
			text-align: right;
		}
		.top-selling-product{}
		.top-selling-product img{
			width: 80px;
			height: auto;
			float: left;
		}
	</style>
	<br>
	
	<div class="row">			
		<div class="col-md-3">
			<div style="background: #f1e8e8" class="dbx">
				<p style="font-size: 24px;">{{$today_total_bus}}</p>
				<p style="font-size: 24px; color: #949292"><i class="fa fa-bus"></i></p>
				<p>Today's Total Bus</p>
			</div>
		</div>						
		<div class="col-md-3">
			<div style="background: #c7ff86" class="dbx">
				<p style="font-size: 24px;">{{$today_total_ticket}}</p>
				<p style="font-size: 24px; color: #75b130"><i class="fa fa-ticket"></i></p>
				<p>Today's Total Ticket </p>
			</div>
		</div>			
		<div class="col-md-3">
			<div style="background: #64e6f7" class="dbx">
				<p style="font-size: 24px;">{{$today_total_fare}}</p>
				<p style="font-size: 24px; color: #2196f3"><i class="fa fa-money"></i></p>
				<p>Today's Total Fare</p>
			</div>
		</div>				
		<div class="col-md-3">
			<div style="background: #ffee5b;" class="dbx">
				<p style="font-size: 24px;">{{$total_route}}</p>
				<p style="font-size: 24px; color: #b1a434"><i class="fa fa-chevron-circle-right"></i></p>
				<p>Total Bus Route</p>
			</div>
		</div>
		
	</div>


	<div class="row" style="margin-top: 15px;">			
		<div class="col-md-6">
			<div style="" class="">
				<p style="font-size: 15px;margin-bottom: 0px;background: #33b5e5;color: #fff;padding: 0px 5px;">Ticket reserveed & confirmed by Passenger</p>
				<table class="table table-bordered">
				    <thead>
				      <tr>
				        <th>Name</th>
				        <th style="text-align: right;">Reserved</th>
				        <th>Confirmed</th>
				      </tr>
				    </thead>
				    <tbody>
				    @foreach($tickets as $ticket)
				      <tr>
				        <td>{{$ticket->passenger_name}}</td>
                          <td style="text-align: right;">@if($ticket->payment_verification_code != null) <span style="color:green;font-weight: bolder;">&#10003;</span> @else <span style="color:red;font-weight: bolder;">X</span> @endif</td>
				        <td>@if($ticket->payment_verification_code == null) <span style="color:green;font-weight: bolder;">&#10003;</span> @else <span style="color:red;font-weight: bolder;">X</span> @endif</td>
				      </tr>
				    @endforeach
				    </tbody>
				  </table>
			</div>
		</div>						
		<div class="col-md-3">
			<div style="background: #d1d8ff" class="dbx">
				<p style="font-size: 24px;">{{$total_reserved_ticket}}</p>
				<p style="font-size: 24px; color: #949bc1"><i class="fa fa-check"></i></p>
				<p>Total Reserved Ticket </p>
			</div>
		</div>						
		<div class="col-md-3">
			<div style="background: #adfb53" class="dbx">
				<p style="font-size: 24px;">{{$total_confirmed_ticket}}</p>
				<p style="font-size: 24px; color: #369331"><i class="fa fa-check-square"></i></p>
				<p>Total Confirmed Ticket</p>
			</div>
		</div>	
	</div>

	<div class="row" style="margin-top: 15px;">	
		<div class="col-md-3">

			<div class="">
				<p style="font-size: 15px;margin-bottom: 0px;background: #33b5e5;color: #fff;padding: 0px 5px;">Latest Ticket</p>
				@foreach($tickets as $ticket)
				<div class="top-selling-product">
					
                    <p style="border-bottom: 1px solid #efecec;">{{$ticket->passenger_name}} [ {{$ticket->passenger_contact_no}} ]<br>[ {{$ticket->tickets}} ] [ Fare: {{$ticket->total_fare}} ]</p>
				</div>
				@endforeach
			</div>
		</div>			
		<div class="col-md-9">
			<div style="" class="">
				<p style="font-size: 15px;margin-bottom: 0px;background: #33b5e5;color: #fff;padding: 0px 5px;">Buses</p>
				<table class="table table-bordered">
				    <thead>
				      <tr>
				        <th>ID</th>
				        <th>Bus Model</th>
				        <th>Bus Route</th>
				        <th>Depture Date</th>
				        <th>Depture Time</th>
				        <th>Distance</th>
				      </tr>
				    </thead>
				    <tbody>
				    	@foreach($busses as $bus)
				        <tr class="">
				          <td>{{$bus->id}}</td>
				          <td>{{$bus->bus_model}}</td>
				          <td>{{$bus->route_relation->route_through}}</td>
				          <td style="text-align:right;">{{\Carbon\Carbon::parse($bus->journey_date)->format('d-m-Y')}}</td>
				          <td style="text-align:right;">{{\Carbon\Carbon::parse($bus->depture_time)->format('H:i A')}}</td>
				          <td style="text-align:right;">{{$bus->route_relation->distance}}</td>
				      	</tr>
				      	@endforeach
				    </tbody>
				  </table>
			</div>
		</div>	
	</div>
	<div style="float:right;">
		
	</div>
    <div class="page">
      
    </div>
  </div>
  
</div>

@stop


@section('script')
	
@stop
