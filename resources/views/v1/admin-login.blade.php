<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>GirlyShopper | Admin Login & Register</title>
    <link rel="stylesheet" href="/v1/css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="/v1/css/style.css">
  </head>


<body>
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->

<!-- Form Module-->
<div class="module form-module">
   <a href="/access/register"><div class="toggle"><i class="fa fa-times fa-user-plus"></i>
    <div class="tooltip">Admin Register</div>
  </div>
  </a>
  <div class="form">
    <h2>Login to your account</h2>
	{{ session('error_message') }}
    <form action="/access" method="post">
	 {{csrf_field()}}
      {{ $errors->first('username') }}
      <input type="text" name="username" placeholder="Username" value="{{ old('username') }}"/>
      
	  {{ $errors->first('password') }}
      <input type="password" name="password" placeholder="Password"/>
      <button name="login" value="submit">Login</button>
    </form>
  </div>
  
  <div class="cta"><a href="">Forgot your password?</a>   <a style="cursor: pointer;text-decoration: underline;" href="/">Home</a>
</div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='js/da0415260bc83974687e3f9ae.js'></script>

        <script src="/v1/js/index.js"></script>

    
    
    
  </body>
</html>