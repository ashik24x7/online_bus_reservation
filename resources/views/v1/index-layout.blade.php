<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Ticket Booking | @yield('title')</title>
    <link rel="stylesheet" href="/v1/css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="/layout/css/font-awesome.min.css">
    <link rel="stylesheet" href="/layout/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/v1/css/style.css">
    <link rel="stylesheet" href="/v1/css/app.css">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/logo/logo.png">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style type="text/css">
    	
    </style>
    @yield('head')
  </head>

<body>

    
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->

<div class="container">
	
<!-- Form Module-->
<div class="module form-module view">
	<div class="container">
		<div class="row" style="margin-right: 10px;">
			
			<div class="col-md-12">
				@yield('body')
			<div>
		<div>
	<div>
</div>
   
	<script src='/v1/js/da0415260bc83974687e3f9ae.js'></script>
	<script src="/v1/js/index.js"></script>
	<script src="/layout/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- Add your site or application content here -->
	<script src="/layout/js/vendor/modernizr-3.5.0.min.js"></script>
	
	<script>window.jQuery || document.write('<script src="/layout/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
	<script src="/layout/js/plugins.js"></script>
	<script src="/layout/js/main.js"></script>

	<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
	<script>
		window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
		ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
	</script>
	<script src="https://www.google-analytics.com/analytics.js" async defer></script>
	
	@yield('script')
    
    
    
  </body>
</html>
