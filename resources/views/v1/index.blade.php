<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Bus Ticket Booking</title>
    <link rel="stylesheet" href="/v1/css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="/layout/css/font-awesome.min.css">
    <link rel="stylesheet" href="/layout/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/v1/css/style.css">
    <link rel="stylesheet" href="/v1/css/app.css">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/logo/logo.png">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style type="text/css">
    	
    </style>
  </head>

<body>
<style type="text/css">
    h1 {
        color: #63c128;;
        padding: 57px 0px 80px;
    }
    input{
        border: 21px solid #93CD6F;
    }
    #from,#to,#date,#search {
        padding: 20px;
        border: 2px solid #7DC354;
        background: #ffffff8f;
        font-size: 18px;
    }
    #search{
        color: blueviolet;
    }
</style>
    
<!--Start Nav-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">BusTickets.com</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/access">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link pull-right" href="/verify-payment">Verifiy Payment</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"></a>
      </li>
    </ul>
  </div>
</nav>
<!--End Nav-->

<!-- Form Module-->
	<div class="container-fluid">
		 <div class="row justify-content-md-center" style="background:url('/images/bus2.png') center no-repeat">
		     <div class="col-md-12">
		         <h1 style="text-align: center;">Buy Bus Ticket Online</h1>
		     </div>
			<div class="col-md-12">
                <div style="padding: 0px 0px 0px 150px;">
                    
                    <form class="form-inline" role="form" method="post" action="{{ url('/available-bus') }}">
			            {{ csrf_field() }}
                      <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPassword2" class="sr-only">From</label>
                        <input type="text" class="form-control" id="from" name="from" placeholder="From">
                      </div>
                      <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPassword2" class="sr-only">To</label>
                        <input type="text" class="form-control" id="to" name="to" placeholder="To">
                      </div>
                      <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPassword2" class="sr-only">Journey Date</label>
                        <input type="date" class="form-control" id="date" name="journey_date" placeholder="Journey Date">
                      </div>
                      <button type="submit" class="btn btn-primary mb-2" id="search">Search</button>
                    </form>
                </div>
			</div>
		<div>
   
   
        <div class="row" style="padding-top:225px;">
           
            <div class="col-md-6" style="background: #FFF;color: #000;font-size: 18px;padding: 20px;text-align: center;">For Telephone booking service <br><span style="font-size:24px;font-weight:bold">Please Call </span></div>
            <div class="col-md-6" style="background:#78C14D;color:#FFF;text-align: center;font-size: 5em;">1645</div>
        </div>
        
        <div class="row">
            <div class="col-md-12" style="color:red">
                    <ul style="list-style: disc;color: #000000;font-size: 16px;padding: 6px 2px;margin: 0px;">
                       @foreach($buses as $bus)
                           <li>{{$bus->route_relation->route_through}} [{{\Carbon\Carbon::parse($bus->journey_date)->format('d-m-Y')}} - {{\Carbon\Carbon::parse($bus->depture_time)->format('H:i A')}}]</li>
                       @endforeach
                    </ul>
            </div>
        </div>
   
   
    </div>
   
	<script src='/v1/js/da0415260bc83974687e3f9ae.js'></script>
	<script src="/v1/js/index.js"></script>
	<script src="/layout/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- Add your site or application content here -->
	<script src="/layout/js/vendor/modernizr-3.5.0.min.js"></script>
	
	<script>window.jQuery || document.write('<script src="/layout/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
	<script src="/layout/js/plugins.js"></script>
	<script src="/layout/js/main.js"></script>

	<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
	<script>
		window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
		ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
	</script>
	<script src="https://www.google-analytics.com/analytics.js" async defer></script>
	
	@yield('script')
    
    
    
  </body>
</html>


