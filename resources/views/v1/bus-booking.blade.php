@extends('/v1/index-layout')

@section('title')
    Home
@stop
<style>
    .bus{
        background: #e9e9e9;
        width: 243px;
        margin: 0px auto;
    }
    .bus .seat{
        width: 50px;
        height: 29px;
        background: #39bdec;
        display: inline-block;
        padding: 5px;
        margin: 4px 4px;
        text-align: center;
        color: #252525;
        font-weight: bold;
    }
    .ticket > ul{
        
    }
    .ticket > ul >li{
        padding: 5px;
        border-bottom: 1px solid #e9e9e9;
    }
</style>

<style type="text/css">
    
    .well{
        background-color: #ddd;padding:20px;border-radius: 5px;
    }

    div label input {
       margin-right:100px;
    }
    body {
        font-family:sans-serif;
    }

    #ck-button {
        margin:4px;
        background-color:#EFEFEF;
        border-radius:4px;
        border:1px solid #D0D0D0;
        overflow:hidden;
        float:left;
    }

    #ck-button label:hover {
        background:#30d8bb;
    }

    #ck-button label {
        float:left;
        width:4.0em;
        background-color: #39bdec;
        margin-bottom: 0;cursor: pointer;
        margin: 5px;
    }

    #ck-button label span {
        text-align:center;
        padding:3px 0px;
        display:block;
    }

    #ck-button label input {
        position:absolute;
        top:-20px;
    }

    #ck-button input:checked + span {
        background-color:#28a745;
        color:#fff;
    }
    #ck-button input:disabled + span {
        background-color:#d81717;
        color:#fff;
    }
</style>
@section('body')
	<br>
    <h2>Select Your Preferred Seat</h2>
    {{ session('error_message')}}
    {{ session('success_message')}}
	<div class="row">
        <div class="col-md-6">
	        <div id="ck-button">
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="A1" @php if($buses->seat_A1 !== null){ @endphp disabled @php } @endphp><span>A1</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="A2" @php if($buses->seat_A2 !== null){ @endphp disabled @php } @endphp><span>A2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="A3" @php if($buses->seat_A3 !== null){ @endphp disabled @php } @endphp><span>A3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="A4" @php if($buses->seat_A4 !== null){ @endphp disabled @php } @endphp><span>A4</span>
					   </label>
					</div>
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="B1" @php if($buses->seat_B1 !== null){ @endphp disabled @php } @endphp><span>B1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="B2" @php if($buses->seat_B2 !== null){ @endphp disabled @php } @endphp><span>B2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="B3" @php if($buses->seat_B3 !== null){ @endphp disabled @php } @endphp><span>B3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="B4" @php if($buses->seat_B4 !== null){ @endphp disabled @php } @endphp><span>B4</span>
					   </label>
					</div>
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="C1" @php if($buses->seat_C1 !== null){ @endphp disabled @php } @endphp><span>C1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="C2" @php if($buses->seat_C2 !== null){ @endphp disabled @php } @endphp><span>C2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="C3" @php if($buses->seat_C3 !== null){ @endphp disabled @php } @endphp><span>C3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="C4" @php if($buses->seat_C4 !== null){ @endphp disabled @php } @endphp><span>C4</span>
					   </label>
					</div>
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="D1" @php if($buses->seat_D1 !== null){ @endphp disabled @php } @endphp><span>D1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="D2" @php if($buses->seat_D2 !== null){ @endphp disabled @php } @endphp><span>D2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="D3" @php if($buses->seat_D3 !== null){ @endphp disabled @php } @endphp><span>D3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="D4" @php if($buses->seat_D4 !== null){ @endphp disabled @php } @endphp><span>D4</span>
					   </label>
					</div>
					
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="E1" @php if($buses->seat_E1 !== null){ @endphp disabled @php } @endphp ><span>E1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="E2" @php if($buses->seat_E2 !== null){ @endphp disabled @php } @endphp><span>E2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="E3" @php if($buses->seat_E3 !== null){ @endphp disabled @php } @endphp><span>E3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="E4" @php if($buses->seat_E4 !== null){ @endphp disabled @php } @endphp><span>E4</span>
					   </label>
					</div>
					
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="F1" @php if($buses->seat_F1 !== null){ @endphp disabled @php } @endphp><span>F1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="F2" @php if($buses->seat_F2 !== null){ @endphp disabled @php } @endphp><span>F2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="F3" @php if($buses->seat_F3 !== null){ @endphp disabled @php } @endphp><span>F3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="F4" @php if($buses->seat_F4 !== null){ @endphp disabled @php } @endphp><span>F4</span>
					   </label>
					</div>
					
					
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="G1" @php if($buses->seat_G1 !== null){ @endphp disabled @php } @endphp><span>G1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="G2" @php if($buses->seat_G2 !== null){ @endphp disabled @php } @endphp><span>G2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="G3" @php if($buses->seat_G3 !== null){ @endphp disabled @php } @endphp><span>G3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="G4" @php if($buses->seat_G4 !== null){ @endphp disabled @php } @endphp><span>G4</span>
					   </label>
					</div>
					
					
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="H1" @php if($buses->seat_H1 !== null){ @endphp disabled @php } @endphp><span>H1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="H2" @php if($buses->seat_H2 !== null){ @endphp disabled @php } @endphp><span>H2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="H3" @php if($buses->seat_H3 !== null){ @endphp disabled @php } @endphp><span>H3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="H4" @php if($buses->seat_H4 !== null){ @endphp disabled @php } @endphp><span>H4</span>
					   </label>
					</div>
					
					
					<div class="col-md-12">
						<label>
					      <input type="checkbox" name="seat" class="check-seat" value="I1" @php if($buses->seat_I1 !== null){ @endphp disabled @php } @endphp><span>I1</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="I2" @php if($buses->seat_I2 !== null){ @endphp disabled @php } @endphp><span>I2</span>
					   </label>
					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="I3" @php if($buses->seat_I3 !== null){ @endphp disabled @php } @endphp><span>I3</span>
					   </label>

					   <label>
					      <input type="checkbox" name="seat" class="check-seat" value="I4" @php if($buses->seat_I4 !== null){ @endphp disabled @php } @endphp><span>I4</span>
					   </label>
					</div>
					
					
				</div>
				   
            </div>
            
            
		<div class="col-md-6">
		<form role="form" method="post" action="{{ url('/confirm-ticket') }}">
			<div class="ticket">
			    <p>
			    </p>
			    <div class="hidden_form">
			        
			    </div>
			</div>
			<div class="ticket-details">
                <p style="color:red">{{ $errors->first('hidden_seat') }}</p>
                <p style="color:red">{{ $errors->first('passenger_name') }}</p>
                <p style="color:red">{{ $errors->first('passenger_contact_no') }}</p>
<!--			    <form role="form" method="post" action="{{ url('/confirm-payment') }}"> -->
			        {{ csrf_field() }}
			        <div class="form-horizontal biller">
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                              <input type="hidden" name="bus_no" class="form-control" id="inputPassword" placeholder="" value="1">
                            </div>
                        </div>
                        
                        
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Passenger Name</label>
                            <div class="col-sm-6">
                              <input type="text" name="passenger_name" class="form-control" id="inputPassword" placeholder="">
                            </div>
                        </div>
                        
                        
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Passenger Contact No</label>
                            <div class="col-sm-6">
                              <input type="text" name="passenger_contact_no" class="form-control" id="inputPassword" placeholder="">
                            </div>
                        </div>
                           
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                              <input type="submit" value="Confirm" class="form-control" name="submit" style="cursor:pointer;background: #33b5e5;color: #fff;">
                            </div>
                        </div>
                        
                        
                    </div>
                </form>
			</div>
		</div>
		
		
		
		
		
	</div>
    
	
	
	<div style="float:right;">
		
	</div>
    <div class="page">
      <p onclick="goBack()" style="cursor: pointer;text-decoration: underline;">&lt;&lt; Go Back</p>

        <script>
        function goBack() {
          window.history.back();
        }
        </script>
    </div>
  </div>
  
</div>

@stop


@section('script')
	<script>
		$(document).ready(function(){
            var counter = 0;
            
			$(".seat").click(function(){
                counter++;
                if (counter % 2 === 0 && $(this).css("background-color") == '#66bd7a'){
                    $(this).css("background-color", "66bd7a");
                } 
                
                
                //$(this).css("background-color", "#66bd7a");
				$(".ticket>p").append('<input type="hidden" name="'+$(this).text()+'" value="'+$(this).text()+'" /> <li>'+$(this).text()+'<div class="btn btn-secondary ashik" style="padding: 0px;width: 20px;margin-left: 15px;background: red;font-size: 12px;">X</div>'+'</li>');
				
			});
            
            $(".ashik").click(function(){
                console.log(3);
			});
		
          
        });
		
		
	</script>
	
	<script type="text/javascript">
	
        $('.check-seat').click(function(){
            if ($(this).prop('checked')==true){
                
                var seat = [];
                $.each($("input[name='seat']:checked"), function(){            
                    seat.push($(this).val());
                });
                
                $(".ticket>p").text(seat.join(", "));
                $(".hidden_form").html('<input type="hidden" name="hidden_seat" value="'+seat.join(", ")+'" />');
            }else{
                var seat = [];
                $.each($("input[name='seat']:checked"), function(){            
                    seat.push($(this).val());
                });
                
                $(".ticket>p").text(seat.join(", "));
                $(".hidden_form").html('<input type="hidden" name="hidden_seat" value="'+seat.join(", ")+'" />');
            }
        })
    </script>
@stop
