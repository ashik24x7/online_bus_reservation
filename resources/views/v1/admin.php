<?php session_start(); ?>
<?php require_once("includes/db.php"); ?>
<?php require_once("includes/function.php"); ?>
<?php 
  if (isset($_SESSION["admin"])) {
        $_SESSION["admin"] = NULL;
    }
 ?>
<?php 
    if (isset($_REQUEST["login"])) {
        $fields = array("username","password");
        $errors = array();
        foreach($fields as $field){
            if (empty($_REQUEST[$field])) {
                $errors[$field] = "<p class=\"error\">* ".ucfirst(remove_under_score($field)." is empty</p>");
             }
        }

        if (empty($errors)) {
            $username = $_REQUEST["username"];
            $password = $_REQUEST["password"];
            $query = "SELECT * FROM admin WHERE username = '{$username}' AND password = '{$password}'";
            $result = mysqli_query($connection,$query);
            

            if ($result) {
              $admin = mysqli_fetch_assoc($result);
              if (isset($_REQUEST["username"],$_REQUEST["password"]) && $_REQUEST["username"] == $admin["username"] && $_REQUEST["password"] == $admin["password"]) {
                  $_SESSION["admin"] = $admin["username"];
                  redirect_to("add.php");
              }else{
                  $message = "<p class=\"message_error\">Username/Password Wrong!</P>";
              }
            }else{
                $message = "<p class=\"message_error\">Login query failed!</P>";
            }
        }else{
            #else what will happen....
        }
    }else{

    }

 ?>

<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Login Page</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/style.css">
  </head>


<body>
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->
<div class="pen-title">
  <span>Pen <i class='fa fa-paint-brush'></i> + <i class='fa fa-code'></i> by <a href='#'>Ashik</a></span>
</div>
<!-- Form Module-->
<div class="module form-module">
  <div class="toggle"><i class="fa fa-times fa-user-plus"></i>
    <div class="tooltip">New account</div>
  </div>
  <div class="form">
    <h2>Login to your account</h2>
    <?php if(isset($message)) {echo $message;} ?>
    <form action="admin.php" method="post">
      <?php if(!empty($errors["username"])){echo $errors["username"];} ?>
      <input type="text" name="username" placeholder="Username" value="<?php if (isset($_REQUEST["username"])){ echo $_REQUEST["username"];} ?>"/>
      <?php if(!empty($errors["password"])){echo $errors["password"];} ?>
      <input type="password" name="password" placeholder="Password"/>
      <button name="login" value="submit">Login</button>
    </form>
  </div>
  <div class="form">
    <h2>Create an account</h2>
    <form>
      <input type="text" placeholder="First Name"/>
      <input type="text" placeholder="Last Name"/>
      <input type="text" placeholder="Username"/>
      <input type="password" placeholder="Password"/>
      <button>Register</button>
    </form>
  </div>
  <div class="cta"><a href="http://andytran.me">Forgot your password?</a></div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='js/da0415260bc83974687e3f9ae.js'></script>

        <script src="js/index.js"></script>

    
    
    
  </body>
</html>
