<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Ticket Booking | @yield('title')</title>
    <link rel="stylesheet" href="/v1/css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="/layout/css/font-awesome.min.css">
    <link rel="stylesheet" href="/layout/bootstrap/css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="/v1/css/style.css">-->
    <link rel="stylesheet" href="/v1/css/app.css">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/logo/logo.png">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style type="text/css">
    	
    </style>
        <style>
        @media all {

            .table{}
            .table tr{
                height: 30px;
            }
            .table td, .table th {
                padding: 0px 5px;
            }
            .deliver_section{
                margin-bottom: 10px;
            }
            .deliver_section p{
                margin-bottom: 0px;
            }
            .table tr,td{
                border-bottom: 0px solid #ced4da;
            }
            .table td:first-child{
                border-right: 1px solid #ced4da;
            }
            .table td:last-child, .table th:last-child{text-align: left;}
            .table tr,td{
                border: 1px solid #33b5e5;
            }
            
        }
        @media print{    
            .no-print, .no-print *{
                display: none !important;
            }
            *{
                font-size: 24px !important;
                color: #000;
            }
            .table tr,td{
                border-bottom: 0px solid #ced4da;
            }
            .table tr,td{
                border: 1px solid #ced4da;
            }
            
            .col-md-6 {
                max-width: 50%;
                float: left;
            }

        }
        .form-control{
            padding: 0px;
        }
        .table td:last-child{text-align: left !important;}
        .table > tbody > tr:first-child {
            background: #ffffff !important;
            color: #666 !important;
        }
        .table td, .table th {
            border-top: 1px solid #33b5e5 !important;
        }
        .bus{
            background: #e9e9e9;
            width: 243px;
            margin: 0px auto;
        }
        .bus .seat{
            width: 50px;
            height: 29px;
            background: #39bdec;
            display: inline-block;
            padding: 5px;
            margin: 4px 4px;
            text-align: center;
            color: #252525;
            font-weight: bold;
        }
        .ticket > ul{

        }
        .ticket > ul >li{
            padding: 5px;
            border-bottom: 1px solid #e9e9e9;
        }
    </style>

    <style type="text/css">

        .well{
            background-color: #ddd;padding:20px;border-radius: 5px;
        }

        div label input {
           margin-right:100px;
        }
        body {
            font-family:sans-serif;
        }

        #ck-button {
            margin:4px;
            background-color:#EFEFEF;
            border-radius:4px;
            border:1px solid #D0D0D0;
            overflow:hidden;
            float:left;
        }

        #ck-button label:hover {
            background:#30d8bb;
        }

        #ck-button label {
            float:left;
            width:4.0em;
            background-color: #39bdec;
            margin-bottom: 0;cursor: pointer;
            margin: 5px;
        }

        #ck-button label span {
            text-align:center;
            padding:3px 0px;
            display:block;
        }

        #ck-button label input {
            position:absolute;
            top:-20px;
        }

        #ck-button input:checked + span {
            background-color:#28a745;
            color:#fff;
        }
</style>
</head>

<body>

    
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->

<div class="container">
	
<!-- Form Module-->
<div class="module form-module view">
	<div class="container" id="print">
		<div class="row" style="margin-right: 10px;">
			
			<div class="col-md-12">
           
	<br>
    <img src="/images/logo/logo.png" alt="">
    <h2>Verify Payment</h2>
    <p style="color:red">{{ session('error_message')}}</p>
    <p style="color:green">{{ session('success_message')}}</p>
    <p style="color:red">{{ $errors->first('ticket_number') }}</p>
	<div class="row" id="print">
		<div class="col-md-6" style="margin:0px auto !important">
			<div class="ticket" >
			    
			</div>
			<div class="ticket-details no-print">
			   <form role="form" method="post" action="{{ url('/verify-payment') }}">
			        {{ csrf_field() }}
			        <div class="form-horizontal biller">
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                              <input type="hidden" name="bus_no" class="form-control" id="inputPassword" placeholder="" value="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Ticket Number</label>
                            <div class="col-sm-6">
                              <input type="text" name="ticket_number" class="form-control" id="inputPassword" placeholder="" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                              <input type="submit" value="Confirm" class="form-control" name="submit" style="cursor:pointer;background: #33b5e5;color: #fff;">
                            </div>
                        </div>
                    </div>
                </form>
			</div>
        </div>
        
		
	<div style="float:right;">
		
	</div>
    <div class="page">
        
    </div>
    
		    
		  <div>
		<div>
	<div>
</div>
   
	<script src='/v1/js/da0415260bc83974687e3f9ae.js'></script>
	<script src="/v1/js/index.js"></script>
	<script src="/layout/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- Add your site or application content here -->
	<script src="/layout/js/vendor/modernizr-3.5.0.min.js"></script>
	
	<script>window.jQuery || document.write('<script src="/layout/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
	<script src="/layout/js/plugins.js"></script>
	<script src="/layout/js/main.js"></script>

	<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
	<script>
		window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
		ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
	</script>
	<script src="https://www.google-analytics.com/analytics.js" async defer></script>
	<script>
		$(document).ready(function(){
            var counter = 0;
            
			$(".seat").click(function(){
                counter++;
                if (counter % 2 === 0 && $(this).css("background-color") == '#66bd7a'){
                    $(this).css("background-color", "66bd7a");
                } 
                
                
                //$(this).css("background-color", "#66bd7a");
				$(".ticket>p").append('<input type="hidden" name="'+$(this).text()+'" value="'+$(this).text()+'" /> <li>'+$(this).text()+'<div class="btn btn-secondary ashik" style="padding: 0px;width: 20px;margin-left: 15px;background: red;font-size: 12px;">X</div>'+'</li>');
				
			});
            
            $(".ashik").click(function(){
                console.log(3);
			});
		
          
        });
		
		
	</script>
	
	<script type="text/javascript">
	
        $('.check-seat').click(function(){
            if ($(this).prop('checked')==true){
                
                var seat = [];
                $.each($("input[name='seat']:checked"), function(){            
                    seat.push($(this).val());
                });
                
                $(".ticket>p").text(seat.join(", "));
                $(".hidden_form").html('<input type="hidden" name="hidden_seat" value="'+seat.join(", ")+'" />');
            }else{
                var seat = [];
                $.each($("input[name='seat']:checked"), function(){            
                    seat.push($(this).val());
                });
                
                $(".ticket>p").text(seat.join(", "));
                $(".hidden_form").html('<input type="hidden" name="hidden_seat" value="'+seat.join(", ")+'" />');
            }
        })
    </script>
  </body>
</html>
