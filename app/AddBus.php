<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\BusRoute;
class AddBus extends Model
{
    protected $fillable = [
    	'bus_model',
    	'bus_type',
    	'bus_route',
    	'journey_date',
    	'depture_time',
    	'arrival_time',
    	'fare',
        
    	'seat_A1',
    	'seat_A2',
    	'seat_A3',
    	'seat_A4',
        
    	'seat_B1',
    	'seat_B2',
    	'seat_B3',
    	'seat_B4',
        
    	'seat_C1',
    	'seat_C2',
    	'seat_C3',
    	'seat_C4',
        
    	'seat_D1',
    	'seat_D2',
    	'seat_D3',
    	'seat_D4',
        
    	'seat_E1',
    	'seat_E2',
    	'seat_E3',
    	'seat_E4',
        
    	'seat_F1',
    	'seat_F2',
    	'seat_F3',
    	'seat_F4',
        
    	'seat_G1',
    	'seat_G2',
    	'seat_G3',
    	'seat_G4',
        
    	'seat_H1',
    	'seat_H2',
    	'seat_H3',
    	'seat_H4',
        
    	'seat_I1',
    	'seat_I2',
    	'seat_I3',
    	'seat_I4',
    ];
    
    public function route_relation(){
		return $this->belongsTo('App\BusRoute','bus_route','id');
	}

}
