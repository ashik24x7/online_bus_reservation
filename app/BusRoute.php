<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusRoute extends Model
{
    protected $fillable = [
    	'route_name',
    	'from',
    	'to',
    	'route_through',
    	'distance'
    ];
    
    
}
