<?php

namespace App;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model implements Authenticatable
{
	use \Illuminate\Auth\Authenticatable;
    
	
    protected $fillable = [
        'full_name','username','password','avatar'
    ];

    
    protected $hidden = [
        'password_reset_token','remember_token','password'
    ];

}
