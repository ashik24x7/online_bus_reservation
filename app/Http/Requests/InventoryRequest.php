<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InventoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "product_code" => "required",
			"product_category" => "required",
			"product_color" => "required",
			"product_description" => "required",
			"product_material" => "required",
			"purchase_quantity" => "required",
			"purchase_cost_per_unit" => "required",
			"purchase_cost" => "required",
			"selling_price_per_unit" => "required",
			"transport_cost" => "required",
			"supplier" => "required",
        ];
    }
}
