<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use DB;
use \App\Admin;
use \App\AddBus;
use \App\Ticket;
use \App\BusRoute;

class HomeController extends Controller
{
    
    public function getHomePage(){
        $data['buses'] = AddBus::with('route_relation')->paginate(6);
        return view('v1/index',$data);
        
    }
    public function getHome(){
    	$data['admins'] = Admin::get();
    	$data['busses'] = AddBus::with('route_relation')->latest()->take(10)->get();
    	$data['tickets'] = Ticket::with(['bus','bus.route_relation'])->latest()->take(3)->get();
    	$data['total_route'] = BusRoute::count();


    	$data['today_total_bus'] = AddBus::whereDate('journey_date', DB::raw('CURDATE()'))->count();
    	$data['today_total_ticket'] = Ticket::whereDate('created_at', DB::raw('CURDATE()'))->count();
    	$data['today_total_fare'] = Ticket::whereDate('created_at', DB::raw('CURDATE()'))->sum('total_fare');
    	
    	
    	$data['total_reserved_ticket'] = Ticket::whereDate('created_at', DB::raw('CURDATE()'))->where('payment_verification_code','!=',NULL)->count();
    	$data['total_confirmed_ticket'] = Ticket::whereDate('created_at', DB::raw('CURDATE()'))->where('payment_verification_code','==',NULL)->count();
    	
        return view('v1/home',$data);
    }
    
    public function availableBus(Request $request){
         
		$this->validate($request,[
			'from' => 'required',
			'to' => 'required',
			'journey_date' => 'required'
		]);
        
        $data['buses'] = AddBus::with('route_relation')->whereDate('journey_date',$request->journey_date)->whereHas('route_relation', function($q) use($request){
            $q->where(['from'=>$request->from, 'to' => $request->to]);
        })->get();
        
        return view('v1.available-bus',$data);
    }
    
    public function getBusBooking($id){
        $data['buses'] = AddBus::with('route_relation')->find($id);
        
        return view('v1.bus-booking',$data);
    }
    public function postConfirmTicket(Request $request){
        $this->validate($request,[
    		'hidden_seat' => 'required',
    		'passenger_name' => 'required',
    		'passenger_contact_no' => 'required',
    	]);
        $ticket['passenger_name'] = $request->passenger_name;
        $ticket['passenger_contact_no'] = $request->passenger_contact_no;
        $ticket['payment_verification_code'] = uniqid(15);
        $ticket['uniq_id'] = uniqid(10);
        $ticket['tickets'] = $request->hidden_seat;
        $ticket['bus_id'] = $request->bus_no;
        
        $seats = explode(",",$request->hidden_seat);
        
        $bus = AddBus::find($request->bus_no);
        
        $ticket['total_fare'] = 0;
        
        $ticket_object = Ticket::create($ticket);
        
        foreach($seats as $seat){
            $temp_seat = (string) str_replace(" ","","seat_".$seat);
            
            $bus->$temp_seat = $ticket_object->id;
            
            $ticket_object->total_fare += $bus->fare;
        };
        $ticket_object->save();
        $bus->save();
        return redirect('/confirm-payment/'.$ticket_object->uniq_id.'/');
    }
    public function getConfirmPayment($id){
        $data['data'] = Ticket::with(['bus','bus.route_relation'])->where('uniq_id',$id)->first();
        return view('v1.confirm-payment',$data);
    }
    public function postConfirmPayment(Request $request){
        
        return $request->all();
    }
    
    public function paymentVerification(Request $request){
        $this->validate($request,[
    		'payment_verification_code' => 'required'
    	]);
        
        $ticket = Ticket::find($request->ticket_id);
        
        if($ticket && $ticket->payment_verification_code == $request->payment_verification_code){
            $ticket->payment_verification_code = null;
            $ticket->save();
            return redirect()->back()->with('success_message','Payment has verified successfully');
        }else{
            return redirect()->back()->with('error_message','There was a problem when payment is being made!');
        }
    }
    
    
    public function getVerifyPayment(){
        return view('v1.verify-payment');
    }
    public function postVerifyPayment(Request $request){
        $this->validate($request,[
    		'ticket_number' => 'required'
    	]);
        return redirect('/confirm-payment/'.$request->ticket_number.'/');
    }
    
    
}
