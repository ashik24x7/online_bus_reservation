<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\AddBus;
use \App\BusRoute;
class AddBusController extends Controller
{
     public function getAddBus(){
        $data['routes'] = BusRoute::get();
    	return view('v1.add-bus',$data);
    }
     public function postAddBus(Request $request){
        $this->validate($request,[
			'bus_model' => 'required',
			'bus_type' => 'required',
			'bus_route' => 'required',
			'journey_date' => 'required',
			'depture_time' => 'required',
			'arrival_time' => 'required',
			'fare' => 'required',
		]);
        
        if(AddBus::create($request->all())){
			return redirect()->back()->with('success_message','Bus has added successfully');
		}else{
			return redirect()->back()->withInput()->with('error_message','An error has occur whole adding product color');
		}
         
    }
    
    public function getBusRoute(){
        $data['routes'] = BusRoute::get();
    	return view('v1.bus-route', $data);
    }
    
    public function postBusRoute(Request $request){
		
		$this->validate($request,[
			'route_name' => 'required',
			'from' => 'required',
			'to' => 'required',
			'route_through' => 'required',
			'distance' => 'required',
		]);
		
		
		if(BusRoute::create($request->all())){
            $data['routes'] = BusRoute::get();
			return redirect()->back()->with('success_message','Bus route has added successfully')->with($data);
		}else{
			return redirect()->back()->withInput()->with('error_message','An error has occur whole adding product color');
		}
    }
    
    
    public function getBuses(){
        $data['buses'] = AddBus::with(['route_relation'])->get();
        return view('v1.buses',$data);
    }
    
}
