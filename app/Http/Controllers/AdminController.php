<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Admin;

class AdminController extends Controller
{
    protected $table = "admins";
    public function getAdminLogin(){
		return view('v1.admin-login');
	}
	
	public function postAdminLogin(Request $request){
		$this->validate($request,[
			'username' => 'required|max:10',
			'password' => 'required'
		]);
		
		$data = $request->only('username','password');
        if(\Auth::guard('admin')->attempt($data)){
            return redirect()->intended('/access/home');
        }else{
            return redirect()->back()->withInput()->with('error_message','Username/Password Wrong!');
        }
	}
	
	public function getAdminRegister(){
		return view('v1.admin-register');
	}
	
	public function postAdminRegister(Request $request){

        
		$this->validate($request,[
			'full_name' => 'bail|required|max:20',
			'username' => 'required|max:10|unique:admins',
			'password' => 'required',
			're_password' => 'required|same:password',
			'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);
        
        
        
		$avatar = $request->file('avatar');
		$data = $request->only('full_name','username');
		$data['password'] = bcrypt($request->password);
		$data['avatar'] = uniqid().'.'.$avatar->getClientOriginalExtension();
		
		if($avatar->move(public_path('/avatar'),$data['avatar'])){
            $admin = Admin::create($data); 

			return redirect()->to('/access')->with('success_message','Congrats! '.$admin->full_name.', You have successfully register yourself. Now you can login.');
		}else{
			return redirect()->to('/access/register')->with('error_message','There is something wrong while registering admin');
		}
	}


	public function getAdminLogout(Request $request){
		//Session::flush();
        \Auth::guard('admin')->logout();
        return redirect()->intended('/access'); 
    }
}
