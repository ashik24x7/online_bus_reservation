<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
    	"name",
    	"contact_no",
    	"aditional_contact_no",
    	"address",
    	"point"
    ];
}
