<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
    	'passenger_name',
    	'passenger_contact_no',
    	'total_fare',
    	'bus_id',
    	'tickets',
    	'uniq_id',
    	'payment_verification_code'
    ];
    
    
    public function bus(){
		return $this->hasOne('App\AddBus','id','bus_id');
        
	}
}