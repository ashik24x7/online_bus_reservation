<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddBusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bus_model')->nullable();
            $table->string('bus_type')->nullable();
            $table->integer('bus_route')->unsigned();
            $table->date('journey_date');
            $table->time('depture_time');
            $table->time('arrival_time');
            $table->integer('fare');
            
            
            $table->tinyInteger('seat_A1')->nullable();
            $table->tinyInteger('seat_A2')->nullable();
            $table->tinyInteger('seat_A3')->nullable();
            $table->tinyInteger('seat_A4')->nullable();
            
            $table->tinyInteger('seat_B1')->nullable();
            $table->tinyInteger('seat_B2')->nullable();
            $table->tinyInteger('seat_B3')->nullable();
            $table->tinyInteger('seat_B4')->nullable();
            
            $table->tinyInteger('seat_C1')->nullable();
            $table->tinyInteger('seat_C2')->nullable();
            $table->tinyInteger('seat_C3')->nullable();
            $table->tinyInteger('seat_C4')->nullable();
            
            $table->tinyInteger('seat_D1')->nullable();
            $table->tinyInteger('seat_D2')->nullable();
            $table->tinyInteger('seat_D3')->nullable();
            $table->tinyInteger('seat_D4')->nullable();
            
            $table->tinyInteger('seat_E1')->nullable();
            $table->tinyInteger('seat_E2')->nullable();
            $table->tinyInteger('seat_E3')->nullable();
            $table->tinyInteger('seat_E4')->nullable();
            
            $table->tinyInteger('seat_F1')->nullable();
            $table->tinyInteger('seat_F2')->nullable();
            $table->tinyInteger('seat_F3')->nullable();
            $table->tinyInteger('seat_F4')->nullable();
            
            $table->tinyInteger('seat_G1')->nullable();
            $table->tinyInteger('seat_G2')->nullable();
            $table->tinyInteger('seat_G3')->nullable();
            $table->tinyInteger('seat_G4')->nullable();
            
            $table->tinyInteger('seat_H1')->nullable();
            $table->tinyInteger('seat_H2')->nullable();
            $table->tinyInteger('seat_H3')->nullable();
            $table->tinyInteger('seat_H4')->nullable();
            
            $table->tinyInteger('seat_I1')->nullable();
            $table->tinyInteger('seat_I2')->nullable();
            $table->tinyInteger('seat_I3')->nullable();
            $table->tinyInteger('seat_I4')->nullable();
            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_buses');
    }
}
