@extends('/v1/layout')

@section('title')
    Home
@stop
<style>
    select#bus_type {
        height: 23px;
    }
</style>
@section('body')
	<br>
    <h2>Buses</h2>
    {{ session('error_message')}}
    {{ session('success_message')}}
	<div class="row">
		<div class="col-md-12">
			<table class="table">
					<tbody>
                        <tr>
                            <th>Bus Model</th>
                            <th>Bus Type</th>
                            <th>Bus Route</th>
                            <th>Journey Date</th>
                            <th>Departure Time</th>
                            <th>Arrival Time</th>
                            <th>Fare</th>
                            <th>Details</th>
                        </tr>
                        @foreach($buses as $bus)
                            <tr>
                                <td>{{$bus->bus_model}}</td>
                                <td>{{$bus->bus_type}}</td>
                                <td>{{$bus->route_relation->route_name}}</td>
                                <td>{{$bus->journey_date}}</td>
                                <td>{{$bus->depture_time}}</td>
                                <td>{{$bus->arrival_time}}</td>
                                <td>{{$bus->fare}}</td>
                                
                                <td><button type="submit" class="form-control" style="cursor:pointer;background: #28a745;color: #fff;padding: 0px 5px;font-size: 12px;">Details</button></td>
                            </tr>
                        @endforeach
					</tbody>
				</table>
		</div>
	</div>
    
	
	
	<div style="float:right;">
		
	</div>
    <div class="page">
      
    </div>
  </div>
  
</div>

@stop


@section('script')
	<script>
		$(document).ready(function(){
			
			var data = '<tr class="">'+
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td>'+
				'<div class="form-horizontal biller">'+
					'<div class="form-group">'+
						'<input type="text" class="form-control" id="biller_name" placeholder="">'+
					'</div>'+
				'</div>'+
			'</td>'+
			
			'<td><a href="#" onClick="return confirm(\'Are you really want to delete this item?\')" id="remove"><i class=\'fa fa-trash-o\'></i></a></td>'+
        '</tr>';
			$("#add_another").click(function(){
				$("tbody").append(data);
				
			});
			
			
			$(".table").on('click', '#remove', function () {
				$(this).closest('tr').remove();
			});
			
		});
		
		
	</script>
@stop
